﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efcore2
{
    public class Student
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public int Age { get; set; }


        public int SchoolID { get; set; }
        public Adress Adress { get; set; }
        public List<StudentCours> StudentCours { get; set; }

    }
}
