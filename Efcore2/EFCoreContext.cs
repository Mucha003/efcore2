﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Efcore2
{
    public class EFCoreContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connection = "Data Source= .; Initial Catalog = EFCoreDB; User Id = sa; Password = rootroot;";
            optionsBuilder.UseSqlServer(connection)
                //pour afficher les requettes en console
                .EnableSensitiveDataLogging(true)
                .UseLoggerFactory(new LoggerFactory().AddConsole((category, level) => level == LogLevel.Information && category == DbLoggerCategory.Database.Command.Name, true));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentCours>().HasKey(x => new { x.CoursID, x.StudentID });
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Adress> Adresses { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Cours> Cours { get; set; }
        public DbSet<StudentCours> StudentCours { get; set; }
    }
}
