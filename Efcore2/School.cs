﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efcore2
{
    public class School
    {
        public int ID { get; set; }
        public string Nom { get; set; }

        public List<Student> StudentList { get; set; }
    }
}
