﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efcore2
{
    public class StudentCours
    {
        public int ID { get; set; }

        public int CoursID { get; set; }
        public Cours Cours { get; set; }

        public int StudentID { get; set; }
        public Student Student { get; set; }
    }
}
