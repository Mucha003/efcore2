﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Efcore2
{
    public class Cours
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        
        public List<StudentCours> StudentCours { get; set; }
    }
}
